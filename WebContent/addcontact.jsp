<!DOCTYPE html>
<html lang="en">
<head>
<title>Add Contact</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<%@ page import="java.sql.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="pojo.contactpojo"%>
<%@page language="java" import="java.util.*"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	ResultSet resultset = null;
	if (session.getAttribute("admin") != "admin") {
		response.sendRedirect("index.jsp");
	}
%>

<jsp:useBean id="con1" class="controller.dbcon" scope="page"></jsp:useBean>
<!-- 
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.min.js" type="text/javascript"></script> -->
<script type="text/javascript" src="assets/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/jquery-confirm.js"></script>
<link rel="stylesheet" href="assets/jquery-confirm.css">

<script>
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
<script type="text/javascript">
	$(document).ready(function() {
		refreshgrp();
		refreshcat();
	//	$('#group').on('click', 
	function refreshgrp() {
			$('#group').empty();
			$.get('getgroups', function(response) {

				var s = $('#group');
				s.append('<option disabled="disabled" selected="selected">Select Group</option>');
				$.each(response, function(index, v) {

					s.append('<option value="' + v.grpname + '">'	+ v.grpname + '</option>');

				});

			});
		} 
	//	$('#category').on('click', 
				function refreshcat() {	
			$('#category').empty();
			$.post('getcategory', function(response) {
				var s = $('#category');


				s.append('<option disabled="disabled" selected="selected">Select Category</option>');
				$.each(response, function(index, v) {
 					
					s.append('<option  value="' + v.catname + '">'	+ v.catname + '</option>');

				});

			});  
		} 
				$('#grouplist').on('click', function() {
				 
					$('#view1').modal('show');
					$('#responds tbody').empty();
					$.get('getgroups', function(response) {

						
						
						$.each(response, function(index, v) {

							
							var row = $("<tr></tr>").appendTo("#responds");
		                       $("<td></td>").text(v.grpname).appendTo(row);
		                       $("<td></td>").html('<a class="btn btn-info" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myeditgrpModal" id="editGroup"> <i id="catedit" class="fa fa-pencil"></i></a> <a type="button" class="btn btn-danger" id="delGroup"><i class="fa fa-trash"></i></a>').appendTo(row);
				//			$('#category').append('<option value="' + v.catname + '">'	+ v.catname + '</option>');

						});

					});
				});

				$('#catlist').on('click', function() {
					 
					$('#view').modal('show');
					$('#categorytable tbody').empty();
					$.post('getcategory', function(response) {

						$.each(response, function(index, v) {
							var row = $("<tr></tr>").appendTo("#categorytable");
		                       $("<td></td>").text(v.catname).appendTo(row);
		                       $("<td></td>").html('<a class="btn btn-info" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myeditcatModal" id="editcat"> <i id="catedit" class="fa fa-pencil"></i></a> <a type="button" class="btn btn-danger" id="deleteCat"><i class="fa fa-trash"></i></a>').appendTo(row);
						//	$('#category').append('<option value="' + v.catname + '">'	+ v.catname + '</option>');

						});

					});
				});
				
						
				$('#responds tbody').on('click','#delGroup', function() {
						var dataa = [];
						$(this).closest('tr').find('td').each( function() {
							var t = $(this).text();
							dataa.push(t)

						});
			
						$.confirm({
				              title: 'Delete',
				              content: 'You cannot undo. Do you want to Delete?',
				              icon: 'fa fa-warning',
				              animation: 'zoom',
				              closeAnimation: 'zoom',
				              buttons: {
				                  confirm: {
				                      text: 'Yes, sure!',
				                      btnClass: 'btn-orange',
				                      action: function () {
				                    	  $.get('deleteGroup?grpname=' + dataa[0], function(response) {
				  							 
				  							if (response == "yes") {
				  								$.alert({title:'Alert!',content: 'Group deleted successfully ', type: 'dark',buttons: { OK: function(){ refreshgrp();$('#view1').modal('hide');}} });
				                  				
				  							} else if (response == "exist") {
				  								$.alert({title:'Alert!',content: 'Cant be deleted', type: 'dark',buttons: { OK: function(){$('#view1').modal('show');}} });
				                  				
				  							} else {
				  								alert('try again');
				  								$.alert({title:'Alert!',content: "Try again!", type: 'dark',buttons: { OK: function(){$('#view1').modal('hide');}} });
				                  				
				  							}
				  						});
				                      }
				                  },
				                  cancel: function () { }
				              }
				          });

				    });

						$('#categorytable tbody').on( 'click', '#deleteCat', function() {

							var dataa = [];
							$(this).closest('tr').find('td').each( function() {
								var t = $(this).text();
								dataa.push(t)

							});

							$.confirm({
					              title: 'Delete',
					              content: 'You cannot undo. Do you want to Delete?',
					              icon: 'fa fa-warning',
					              animation: 'zoom',
					              closeAnimation: 'zoom',
					              buttons: {
					                  confirm: {
					                      text: 'Yes, sure!',
					                      btnClass: 'btn-orange',
					                      action: function () {
					                    	  $.get('deleteCategory?categoryname='+ dataa[0], function(response) {
					  							 
					  							if (response == "yes") {
					  								$.alert({title:'Alert!',content: 'Group deleted successfully ', type: 'dark',buttons: { OK: function(){ refreshcat();$('#view').modal('hide');}} });
					                  				
					  							} else if (response == "exist") {
					  								$.alert({title:'Alert!',content: 'Cant be deleted', type: 'dark',buttons: { OK: function(){ $('#view').modal('hide');}} });
					                  				
					  							} else {
					  								$.alert({title:'Alert!',content: "Try again!", type: 'dark',buttons: { OK: function(){ $('#view').modal('hide');}} });
					                  				
					  							}
					  						});
					                      }
					                  },
					                  cancel: function () { }
					              }
					          });
				
						 

						});

						$('#responds tbody').on('click','#editGroup',function() {

							var dataa = [];
							$(this).closest('tr').find('td').each(function() {
								var t = $(this).text();

								dataa.push(t)
							});
						//	$('#oldgrpname').val(dataa[1]);
							$('#updateGroupName').on('click',function(){
								var newg=$('#newgrpname').val();
								$.post('updategrp?oldgrpname='+dataa[0]+'&newgrpname='+newg,function(data){
									$.alert({title:'Alert!',content: data, type: 'dark',buttons: { OK: function(){$refreshgrp();('#newgrpname').val('');$('#myeditgrpModal').modal('hide');$('#view1').modal('hide');}} });
								});
								
							});
						});

						//for edit category

						$('#categorytable tbody').on( 'click', '#editcat', function() {

							var dataa = [];
							$(this).closest('tr').find('td').each( function() {
								var t = $(this).text();

								dataa.push(t)
							});
						//	$('#oldcatname').val(dataa[0]);
							$('#updateCatName').on('click',function(){
								var newg=$('#newcatname').val();
								$.post('updatecategory?oldcatname='+dataa[0]+'&newcatname='+newg,function(data){
									$.alert({title:'Alert!',content: data, type: 'dark',buttons: { OK: function(){ refreshcat();$('#newcatname').val('');$('#myeditcatModal').modal('hide');$('#view').modal('hide');}} });
								});
								
							});
							
							
						});
						$('#AddNewGroup').on('click',function(){
							var newg=$('#NewGroupName').val();
							$.post('AddGroup?grpname='+newg,function(data){
								$.alert({title:'Alert!',content: data, type: 'dark',buttons: { OK: function(){refreshgrp();
									 $('#NewGroupName').val('');
									 $('#view1').modal('hide');refreshgrp();
									}} });
							});
							
						});
						$('#AddNewCat').on('click',function(){
							var newg=$('#NewCatName').val();
							$.post('AddCategory?catname='+newg,function(data){
								$.alert({title:'Alert!',content: data, type: 'dark',buttons: { OK: function(){refreshcat(); $('#NewCatName').val('');$('#view').modal('hide');}} });
							});
							
						});

						$("#fileUpload").on('change', function() {

							//Get count of selected files
							var countFiles = $(this)[0].files.length;

							var imgPath = $(this)[0].value;
							var extn = imgPath
									.substring(
											imgPath
													.lastIndexOf('.') + 1)
									.toLowerCase();
							var image_holder = $("#image-holder");
							image_holder.empty();

							if (extn == "gif" || extn == "png"
									|| extn == "jpg"
									|| extn == "jpeg") {
								if (typeof (FileReader) != "undefined") {

									//loop for each file selected for uploaded.
									for (var i = 0; i < countFiles; i++) {
										var reader = new FileReader();
										reader.onload = function(
												e) {
											$(
													"<img />",
													{
														"src" : e.target.result,
														"class" : "thumb-image"
													})
													.appendTo(
															image_holder);
										}
										image_holder.show();
										reader
												.readAsDataURL($(this)[0].files[i]);
									}

								} else {
									alert("This browser does not support FileReader.");
								}
							} else {
								alert("Pls select only images");
							}
						});

						//img 2 preview
						$("#img2").on( 'change', function() {

							//Get count of selected files
							var countFiles = $(this)[0].files.length;

							var imgPath = $(this)[0].value;
							var extn = imgPath
									.substring(
											imgPath
													.lastIndexOf('.') + 1)
									.toLowerCase();
							var image_holder = $("#image-holder2");
							image_holder.empty();

							if (extn == "gif" || extn == "png"
									|| extn == "jpg"
									|| extn == "jpeg") {
								if (typeof (FileReader) != "undefined") {

									//loop for each file selected for uploaded.
									for (var i = 0; i < countFiles; i++) {
										var reader = new FileReader();
										reader.onload = function(
												e) {
											$(
													"<img />",
													{
														"src" : e.target.result,
														"class" : "thumb-image"
													})
													.appendTo(
															image_holder);
										}
										image_holder.show();
										reader
												.readAsDataURL($(this)[0].files[i]);
									}

								} else {
									alert("This browser does not support FileReader.");
								}
							} else {
								alert("Pls select only images");
							}
						});

						//img 3 preview
						$("#img3").on('change', function() {

							//Get count of selected files
							var countFiles = $(this)[0].files.length;

							var imgPath = $(this)[0].value;
							var extn = imgPath
									.substring(
											imgPath
													.lastIndexOf('.') + 1)
									.toLowerCase();
							var image_holder = $("#image-holder3");
							image_holder.empty();

							if (extn == "gif" || extn == "png"
									|| extn == "jpg"
									|| extn == "jpeg") {
								if (typeof (FileReader) != "undefined") {

									//loop for each file selected for uploaded.
									for (var i = 0; i < countFiles; i++) {
										var reader = new FileReader();
										reader.onload = function(
												e) {
											$(
													"<img />",
													{
														"src" : e.target.result,
														"class" : "thumb-image"
													})
													.appendTo(
															image_holder);
										}
										image_holder.show();
										reader
												.readAsDataURL($(this)[0].files[i]);
									}

								} else {
									alert("This browser does not support FileReader.");
								}
							} else {
								alert("Pls select only images");
							}
						});

						//img 4 preview
						$("#img4").on( 'change', function() {

							//Get count of selected files
							var countFiles = $(this)[0].files.length;

							var imgPath = $(this)[0].value;
							var extn = imgPath
									.substring(
											imgPath
													.lastIndexOf('.') + 1)
									.toLowerCase();
							var image_holder = $("#image-holder4");
							image_holder.empty();

							if (extn == "gif" || extn == "png"
									|| extn == "jpg"
									|| extn == "jpeg") {
								if (typeof (FileReader) != "undefined") {

									//loop for each file selected for uploaded.
									for (var i = 0; i < countFiles; i++) {
										var reader = new FileReader();
										reader.onload = function(e) {
											$(
													"<img />",
													{
														"src" : e.target.result,
														"class" : "thumb-image"
													})
													.appendTo(
															image_holder);
										}
										image_holder.show();
										reader
												.readAsDataURL($(this)[0].files[i]);
									}

								} else {
									alert("This browser does not support FileReader.");
								}
							} else {
								alert("Pls select only images");
							}
						});

						 $("#fileUpload").on("change", function() {
							var files = !!this.files ? this.files
									: [];
							if (!files.length
									|| !window.FileReader)
								return; // no file selected, or no FileReader support

							if (/^image/.test(files[0].type)) { // only image file
								var reader = new FileReader(); // instance of the FileReader
								reader.readAsDataURL(files[0]); // read the local file

								reader.onload = function (e) {
				                    $('#imagePreview')
				                        .attr('src', e.target.result)
				                        .width(150)
				                        .height(50);
				                };

				                reader.readAsDataURL(input.files[0]);
								
								/* reader.onloadend = function() { 
									$("#imagePreview").css("background-image","url("+ this.result+ ")");
								} */
							}
						}); 
						$("#img2").on( "change", function() {
							var files = !!this.files ? this.files
									: [];
							if (!files.length
									|| !window.FileReader)
								return; // no file selected, or no FileReader support

							if (/^image/.test(files[0].type)) { // only image file
								var reader = new FileReader(); // instance of the FileReader
								reader.readAsDataURL(files[0]); // read the local file

								reader.onload = function (e) {
				                    $('#imagePreview2')
				                        .attr('src', e.target.result)
				                        .width(150)
				                        .height(50);
				                };

				                reader.readAsDataURL(input.files[0]);

							/* 	reader.onloadend = function() { 
									$("#imagePreview2").css("background-image","url("+ this.result+ ")");
								} */
							}
						});
						$("#img3").on( "change", function() {
							var files = !!this.files ? this.files
									: [];
							if (!files.length
									|| !window.FileReader)
								return; // no file selected, or no FileReader support

							if (/^image/.test(files[0].type)) { // only image file
								var reader = new FileReader(); // instance of the FileReader
								reader.readAsDataURL(files[0]); // read the local file

								reader.onload = function (e) {
				                    $('#imagePreview3')
				                        .attr('src', e.target.result)
				                        .width(150)
				                        .height(50);
				                };

				                reader.readAsDataURL(input.files[0]);

								/* reader.onloadend = function() { 
									$("#imagePreview3").css("background-image","url("+ this.result+ ")");
								} */
							}
						});
						$("#img4").on( "change", function() {
							var files = !!this.files ? this.files
									: [];
							if (!files.length
									|| !window.FileReader)
								return; // no file selected, or no FileReader support

							if (/^image/.test(files[0].type)) { // only image file
								var reader = new FileReader(); // instance of the FileReader
								reader.readAsDataURL(files[0]); // read the local file

								reader.onload = function (e) {
				                    $('#imagePreview4')
				                        .attr('src', e.target.result)
				                        .width(150)
				                        .height(50);
				                };

				                reader.readAsDataURL(input.files[0]);

								/* reader.onloadend = function() { // set image data as background of div
									$("#imagePreview4").css("background-image","url("+ this.result+ ")");
								} */
							}
						});
			 });

	function FillFields(f) {
		if (f.samefields.checked == true) {
			f.pmobile.value = f.mobile.value;
			f.ptelephone.value = f.telephone.value;
			f.pemail.value = f.email.value;

		}
		if(f.companyfields.checked==true){
			f.cmobile.value=f.mobile.value;
			f.ctelephone.value=f.telephone.value;
			f.cemail.value=f.email.value
		}
	}
</script>

<style>
#imagePreview {
	width: 150px;
	height: 50px;
	background-position: center center;
	background-size: cover;
	-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
	display: block;
}

#imagePreview:hover {
	box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}

#imagePreview2 {
	width: 150px;
	height: 50px;
	background-position: left left;
	background-size: cover;
	-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
	display: block;
}

#imagePreview3 {
	width: 150px;
	height: 50px;
	background-position: left left;
	background-size: cover;
	-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
	display: block;
}

#imagePreview4 {
	width: 150px;
	height: 50px;
	background-position: left left;
	background-size: cover;
	-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
	display: block;
}
</style>

<style type="text/css">
.thumb-image {
	float: center;
	width: 250px;
	height: 250px;
	position: relative;
	padding: 10px;
}
</style>


</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid btn-primary">
			<div class="row">
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="landingpage.jsp">Contact
								Management System</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right btn-default">
						<li><a href="logout"><i class="fa fa-sign-out"></i>
								Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

	<!-- Modal -->
	<div class="modal fade" id="view1">
		<div class="modal-dialog modal-sh">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Contact Management System</h3>
				</div>

				<div class="col-md-12">
					<div class="tabs-container" style="border: 1px solid #927b7b;">
						<ul class="nav nav-tabs">
							<li class="active " ><a data-toggle="tab" href="#tab-2"
								aria-expanded="false">View Group</a></li>

							<li class=""><a data-toggle="tab" href="#tab-1"
								aria-expanded="true"> Add Group</a></li>

						</ul>
						<div class="tab-content">
							<div id="tab-1" class="tab-pane " >
								<div class="panel-body">
										<div class="form-group">
											<label>Group Name</label> <input type="text" name="grpname" id="NewGroupName"
												placeholder="Enter Group name" class="form-control">
										</div>
										<button class="btn btn-primary" id="AddNewGroup">Add</button>
								</div>


							</div>
							<div id="tab-2" class="tab-pane active">
								<div class="panel-body">
									<div class="table-responsive">
										<table id="responds" class="table table-bordered">
											<thead>
												<tr>
													
													<th>Group_Name</th>
													<th>Action</th>

												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>

								</div>



							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<!-- modal ends here -->
	<form action="addcontact" enctype="multipart/form-data" method="post">


		<div class="container-fluid">
			<div class="panel panel-primary">
				<div class="panel-heading bg-info">
					<div class="panel-title">New Contact</div>
					<div
						style="float: right; font-size: 85%; position: relative; top: -28px">
						<a class="btn btn-default" type="button" href="landingpage.jsp">Cancel</a>
						<button id="signinlink" class="btn btn-success" type="submit">Save</button>

					</div>

				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5">
							<div class="col-md-5">
								<div class="fileupload fileupload-new"
									data-provides="fileupload">
									<span class="btn btn-primary btn-file"><span
										class="fileupload-new">Photo</span> 
										<img id="imagePreview" src="#" alt=""/><input
										id="fileUpload" type="file" name="img1" />  </span> <span class="fileupload-preview">
										<a type="button" data-toggle="modal" data-target="#myModal">Preview</a>
									</span>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-2">
								<div class="fileupload fileupload-new "
									data-provides="fileupload">
									<span class="btn btn-primary btn-file"><span
										class="fileupload-new">Bussiness Card</span> <span
										class="fileupload-exists">Front size</span> 
										<img id="imagePreview2" src="#" alt=""/><input type="file"
										name="img2" onchange="readURL(this);" id="img2" /> </span> <span class="fileupload-preview"></span>
									<a type="button" data-toggle="modal" data-target="#myModal2">Preview</a>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-md-offset-1">
							<div class="col-md-5">
								<div class="fileupload fileupload-new"
									data-provides="fileupload">
									<span class="btn btn-primary btn-file"><span
										class="fileupload-new">Bussiness Card</span> <span
										class="fileupload-exists">Back side</span><img id="imagePreview3" src="#" alt=""/>
										 <input type="file"
										name="img3" id="img3" /></span> <span
										class="fileupload-preview"></span> <a type="button" 
										data-toggle="modal" data-target="#myModal3">Preview</a>
										
								</div>
								
							</div>
							<div class="col-md-5 col-md-offset-2">
								<div class="fileupload fileupload-new "
									data-provides="fileupload">
									<span class="btn btn-primary btn-file"><span
										class="fileupload-new">Leaflet</span> <span
										class="fileupload-exists"></span><img id="imagePreview4" src="#" alt=""/> <input type="file"
										name="img4" id="img4" /></span> <span
										class="fileupload-preview"> </span> <a type="button"
										data-toggle="modal" data-target="#myModal4">Preview</a>
								</div>
							</div>
						</div>

					</div>
					<br/>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-2">
									<label class="control-label">Group</label>
								</div>
								<div class="col-md-10">
									<div class="input-group date">
										<select class=" form-control" name="group" id="group" style="text-align: center;">
											 <option disabled="disabled" selected="selected">Select Group</option>
										</select> 
										<span class="input-group-btn"> <a class="btn btn-info" id="grouplist"> <i
												class="fa fa-plus"></i>
										</a>

										</span>
									</div>

								</div>

							</div>
						</div>
						<div class=" col-md-6">
							<div class="form-group">
								<div class="col-md-2">
									<label class="control-label">Ref Notes</label>
								</div>
								<div class="col-md-10">
									<textarea name="refnotes" id="refNotes" class="form-control"
										placeholder="" ></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-2">
									<label class="control-label">Category</label>
									 
								</div>
								<div class="col-md-10">

									<div class="input-group date">
										<select class=" form-control" name="catogary" id="category" style="text-align: center;" required="required">
											 <option disabled="disabled" selected="selected">Select Category</option>

										</select>
										 
										<span class="input-group-btn"> <a class="btn btn-info" id="catlist"> <i
												class="fa fa-plus"></i>
										</a>

										</span>
									</div>

								</div>

							</div>
						</div>

					</div>
				</div>

				<!--first panel ends here-->
				<div class="row">
					<div class="container-fluid">
						<div class="col-md-12">

							<div class="panel panel-primary">
								<div class="panel-heading">
									<div class="panel-title">Contact Info</div>

								</div>

								<div class="panel-body">
									<div class="form-group">
										<div class="row">
											<div class="col-md-5">
												<div id="div_id_catagory">
													<label for="title" class="control-label col-md-1">
														Title</label>
													<div class="controls col-md-3 ">
													<select class=" form-control" name="title" id="title" style="text-align: center;" required="required">
											 <option disabled="disabled" selected="selected">Select title</option>
											 <option value="MR.">Mr.</option>
 											 <option value="MRS.">Mrs.</option>
 											 <option value="MISS">Miss</option>
 											 <option value="MS">MS</option>

										</select>											
											</div>
												</div>
												<div id="div_id_fname">
													<label for="first_name" class="control-label col-md-3">First
														Name</label>
													<div class="controls col-md-5 ">
														<input class="input-md textinput textInput form-control"
															id="first_name" name="fname" placeholder="First Name"
															type="text" />
													</div>
												</div>
											</div>
											<div class="col-md-7">
												<div id="div_id_lname">
													<label for="lastname" class="control-label col-md-2">Last
														Name</label>
													<div class="controls col-md-4">
														<input class="input-md textinput textInput form-control"
															id="lastname" name="lname" placeholder="Last name"
															type="text" />
													</div>
												</div>
												<div id="div_id_nname">
													<label for="nickname" class="control-label col-md-2">Nick
														Name</label>
													<div class="controls col-md-4">
														<input class="input-md textinput textInput form-control"
															id="" nickname"" name="nname" placeholder="Nick name"
															type="text" />
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-5">

												<div id="div_id_number">
													<label for="lastname"
														class="control-label col-md-3  col-md-offset-4">Mobile</label>
													<div class="controls col-md-5">
														<input class="input-md textinput textInput form-control"
															id="mobile" name="mobile" placeholder="Mobile number"
															type="text" />
													</div>
												</div>
											</div>
											<div class="col-md-7">
												<div id="div_id_number">
													<label for="nickname" class="control-label col-md-2">Telephone</label>
													<div class="controls col-md-4">
														<input class="input-md textinput textInput form-control"
															id="tetephone" name="telephone"
															placeholder="Telephone number" type="text" />
													</div>
												</div>
												<div id="div_id_number">
													<label for="emaill̥" class="control-label col-md-2">Email</label>
													<div class="controls col-md-4">
														<input class="input-md textinput textInput form-control"
															id="email" name="email" placeholder="Email Address"
															type="email" />
													</div>
												</div>
												
											</div>
											
										</div>
										<div class="controls col-md-4 col-md-offset-9">
	                                      	<input type="checkbox" name="samefields"
										onclick="FillFields(this.form)"/> <em>Copy to Personal Info</em>&nbsp;&nbsp;
											<input type="checkbox" name="companyfields"
										onclick="FillFields(this.form)"/> <em>Copy to Company</em>&nbsp;&nbsp;
									</div>

									</div>
								</div>




							</div>

						</div>



					</div>
				</div>
				<!--second panel ends here-->

				<!--third panel ends here-->
				<div class="row">
					<div class="container-fluid" >
						<div class="col-md-6" >

							<div class="panel panel-primary" style="margin-bottom: 80px;">
								<div class="panel-heading">
									<div class="panel-title">Personal Info</div>

								</div>
								<div class="panel-body">

								
									<div id="mobile" class="form-group ">
										<label for="mobile" class="control-label col-md-4 ">
											Mobile</label>
										<div class="controls col-md-8 ">
											<input class="input-md  textinput textInput form-control"
												id="id_username" maxlength="30" name="pmobile"
												placeholder="Enter Mobile Number"
												style="margin-bottom: 10px" type="phone" />
										</div>
									</div>
									<div id="telephone" class="form-group ">
										<label for="telephone"
											class="control-label col-md-4  requiredField">
											Telephone </label>
										<div class="controls col-md-8 ">
											<input class="input-md emailinput form-control" id="id_email"
												name="ptelephone" placeholder="Enter Telephone Number"
												style="margin-bottom: 10px" type="phone" />
										</div>
									</div>
									<div id="email" class="form-group ">
										<label for="email"
											class="control-label col-md-4  requiredField">Email</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="pemail" name="pemail" placeholder="Enter Email"
												style="margin-bottom: 10px" type="email" />
										</div>
									</div>
									<div id="im" class="form-group ">
										<label for="im" class="control-label col-md-4  requiredField">
											IM </label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="im" name="pim" placeholder="Enter IM"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="" class="form-group ">
										<label for="" class="control-label col-md-4  requiredField">
											Social Media 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="social_media1" name="socialmedia1"
												placeholder="Enter Social Media 1" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>

									<div id="div_id_company" class="form-group required">
										<label for="id_company"
											class="control-label col-md-4  requiredField"> Social
											Media 2 </label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_company" name="socialmedia2"
												placeholder="Enter Social Media 2" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_catagory" class="form-group required">
										<label for="id_catagory"
											class="control-label col-md-4  requiredField">
											Address Line 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_catagory" name="addsline1"
												placeholder="Enter Address Line 1" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Address Line 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="addsline2" placeholder="Ebter Address Line 2"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField"> City</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="pcity" placeholder="Enter city"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">Post Code</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="postalcode"
												placeholder="Enter post code"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Country</label>
										<div class="controls col-md-8 " style="height: 132px;">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="pcountry" placeholder="Enter country"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>



								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-primary">
								<div class="panel-heading ">
									<div class="panel-title">Company</div>

								</div>
								<div class="panel-body">

									<div id="div_id_username" class="form-group required">
										<label for="id_username"
											class="control-label col-md-4  requiredField">
											Company</label>
										<div class="controls col-md-8 ">
											<input class="input-md  textinput textInput form-control"
												id="id_username" maxlength="30" name="company"
												placeholder="Enter Company Name" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_email" class="form-group required">
										<label for="id_email"
											class="control-label col-md-4  requiredField">Designation
											/ Dept</label>
										<div class="controls col-md-8 ">
											<input class="input-md emailinput form-control" id="id_email"
												name="desgnation" placeholder="Enter Designation"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_catagory" class="form-group required">
										<label for="id_catagory"
											class="control-label col-md-4  requiredField"> Mobile</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_catagory" name="cmobile"
												placeholder="Enter Company Mobile Number"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Telephone</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="ctelephone"
												placeholder="Enter Company Telephone Number"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> Email</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="cemail" placeholder="Enter Email"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">IM</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="cim" placeholder="Enter IM"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> Social
											Media 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="csocialmedia1"
												placeholder="Enter Social Media 1 " style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">Social
											Media 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="csocialmedia2"
												placeholder="Enter Social Media 2" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											Address Line 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="caddsline1"
												placeholder="Enter Address Line 1" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											Address Line 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="caddsline2"
												placeholder="Enter Address Line 2" style="margin-bottom: 10px"
												type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> City</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="ccity" placeholder="Enter City"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											 Post Code</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="zipcode" placeholder="Enter Postcode"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">Country</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="ccountry" placeholder="Enter Country"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div style="float: right; font-size: 85%; top: -10px">
						<button type="submit" class="btn btn-success "
							style="margin-right: 30px; margin-top: -80px;">Save</button>
					</div>
				</div>
				<!--third panel ends here-->


			</div>
			<!--panel body ends here-->

		</div>

		</div>

	</form>
	<!-- Modal -->
	<div class="modal fade" id="view">
		<div class="modal-dialog modal-sh">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Contact Management System</h3>
				</div>
				<div class="col-md-12">
					<div class="tabs-container" style="border: 1px solid #927b7b;">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#tab-22"
								aria-expanded="true">View Category</a></li>
							<li class=""><a data-toggle="tab" href="#tab-11"
								aria-expanded="false"> Add Category</a></li>


						</ul>
						<div class="tab-content">

							<div id="tab-22" class="tab-pane active">
								<div class="panel-body">
									<div class="table-responsive">
										<table id="categorytable" class="table table-bordered">
											<thead>
												<tr>
													<th>Category Name</th>
													<th>Action</th>

												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>

								</div>

							</div>



							<div id="tab-11" class="tab-pane">
								<div class="panel-body">
										<div class="form-group">
											<label>Category Name</label> <input type="text"
												name="catname" id="NewCatName" placeholder="Enter Category name"
												class="form-control">
										</div>
										<button class="btn btn-primary" id="AddNewCat">Add</button>
								</div>


							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<!-- modal ends here -->

	<!-- Modal -->
	<div class="modal fade" id="myeditgrpModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Update Group</h4>
				</div>
				<div class="modal-body">
					<!-- <form action="updategrp" method="post"> -->
						 
						<!-- <div class="form-group">
							<input type="hidden" name="oldgrpname" id="oldgrpname" class="form-control">
						</div> -->
						<div class="form-group">
							<label>New Group Name</label> <input type="text" name="newgrpname" id="newgrpname" class="form-control">
						</div>
						<button class="btn btn-primary" id="updateGroupName">Update</button>
					<!-- </form> -->

				</div>
				<div class="modal-footer"></div>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myeditcatModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Update Catogery</h4>
				</div>
				<div class="modal-body">
					<!-- <form action="updatecategory" method="post"> -->
					<!-- 	<div class="form-group">
							<input type="hidden" name="oldcatname" id="oldcatname" class="form-control">
						</div> -->
						<div class="form-group">
							<label>New Category Name</label> <input type="text" name="newcatname" id="newcatname" class="form-control">
						</div>
						<button class="btn btn-primary" id="updateCatName">Update</button>
					<!-- </form> -->

				</div>
				<div class="modal-footer"></div>
			</div>

		</div>
	</div>

	<!--update categoery  -->





	<!-- modals for img preview -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<table>
				<tbody class="col-md-4">
					<tr>
						<td><span id="image-holder" style="height: 1000px"> </span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal2" role="dialog">
		<div class="modal-dialog">
			<table>
				<tbody class="col-md-4">
					<tr>
						<td><span id="image-holder2" style="height: 1000px"> </span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal3" role="dialog">
		<div class="modal-dialog">
			<table>
				<tbody class="col-md-4">
					<tr>
						<td><span id="image-holder3" style="height: 1000px"> </span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="myModal4" role="dialog">
		<div class="modal-dialog">
			<table>
				<tbody class="col-md-4">
					<tr>
						<td><span id="image-holder4" style="height: 1000px"> </span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>






</body>
</html>
