<%@page import="javafx.scene.control.Alert"%>
<%@page import="java.util.ArrayList"%>
<%@page import="pojo.*"%>
<%@page language="java" import="java.util.*"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%
	ResultSet resultset = null;
	if (session.getAttribute("admin") != "admin") {
		response.sendRedirect("index.jsp");
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Search Contact</title>
<meta charset="utf-8">

 
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}

th {
	background-color: #428bca;
	color: white;
}
b{
    font-weight: 700;
    font-size: 14px;
	}
span{
margin-left:5px;

}
</style>

<script type="text/javascript" src="assets/jquery-3.1.1.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript" src="assets/jquery-confirm.js"></script>
<link rel="stylesheet" href="assets/jquery-confirm.css">
<link rel="stylesheet" href="assets/jquery-ui.css">
<!-- <link rel="stylesheet" href="assets/toastr.min.css">
 -->
 <link rel="stylesheet" href="assets/imagezoom.css">
 
<script type="text/javascript" src="assets/jquery-ui.js"></script> 
<script type="text/javascript" src="assets/mydatepicker.js"></script>
<!-- <script type="text/javascript" src="assets/toastr.min.js"></script>
 -->
<script type="text/javascript">


</script>
<script type="text/javascript">
	$(document).ready(function(){
		
		$.get('getgroups', function(response) {
			var s = $('#group');
 
			$.each(response, function(index, v) {
				s.append('<option value="' + v.grpname + '">' + v.grpname + '</option>');

			});

		});
		$.post('getcategory', function(response) {
			var s = $('#category');
 
			$.each(response, function(index, v) {
				s.append('<option value="' + v.catname + '">' + v.catname + '</option>');

			});

		});
		
		$('#contactlist tbody').on('click','#editbtn',function() {
			var data = [];
			$(this).closest('tr').find('td').each(function() {
					var t = $(this).text();

					data.push(t)
		    });

			//	alert(data[0]);
			var form = $('<form action="Updatecontact.jsp" method="post">'
					+ '<input type="hidden" name="sno" value="'+data[0]+'"> </form>');
			$('body').append(form);

			$(form).submit();

		});
		
		$('#contactlist tbody').on('click','#delete',function() {
			var data = [];
			$(this).closest('tr').find('td').each(function() {
					var t = $(this).text();

					data.push(t)
		    });
       //   $.alert(data[3]);
          $.confirm({
              title: 'Delete',
              content: 'You cannot undo. Do you want to Delete?',
              icon: 'fa fa-warning',
              animation: 'zoom',
              closeAnimation: 'zoom',
              buttons: {
                  confirm: {
                      text: 'Yes, sure!',
                      btnClass: 'btn-orange',
                      action: function () {
                      	$.ajax({
                  			url : 'deleteContact',
                  			data : {
                  				id : data[0],
				    			}, 
                  			
                  			success : function(responseText) {
                  				$.alert({title:'Alert!',content: responseText, type: 'dark',buttons: { OK: function(){ location.reload();}} });
                  				 
                  			}
                  		});
                      }
                  },
                  cancel: function () { }
              }
          });
			 

		});
		$('#contactlist tbody').on('click','#view',function() {

			var data = [];
			$(this).closest('tr').find('td').each(function() {
					var t = $(this).text();

					data.push(t);
		    });
			// alert(data[3]);
			$('#viewdetails').modal('show');
			$('#img1').html( "<img width='150'  height='100' src='displayphoto?id=" + data[0] + "'></img>");
			$('#img2').html( "<img width='150'  height='100' src='img2servlet?id=" + data[0] + "'></img>");
			$('#img3').html( "<img width='150'  height='100' src='img3servlet?id=" + data[0] + "'></img>");
			$('#img4').html( "<img width='150'  height='100' src='img4servlet?id=" + data[0] + "'></img>");
			
			$.post('getdetails?id='+data[0], function(response) {
				 
				$.each(response, function(index, v) {

					$('#group1').html(v.grpname);
					$('#category1').html(v.catname);	
					$('#refnotes').html(v.refnotes);
					$('#title1').html(v.title)	;
					$('#fnam').html(v.firstname)	;
					$('#lnam').html(v.lastname)	;
					$('#nickname').html(v.nickname);	
					$('#mobnum').html(v.contmobile)	;
					$('#landline').html(v.telephone)	;
					$('#emailid').html(v.contemail)	;
					$('#pmobile').html(v.prsnlmoble)	;
					$('#ptelephone').html(v.prsnltelephone);	
					$('#pemail').html(v.prsnlemail)	;
					$('#pim').html(v.prsnlim)	;
					$('#psm1').html('<a href="https://www.facebook.com/'+v.socialmedia1+'" target="_blank">'+v.socialmedia1+'</a>')	;
					$('#psm2').html('<a href="https://www.twitter.com/'+v.socialmedia2+'" target="_blank">'+v.socialmedia2+'</a>')	;
					$('#padds1').html(v.addsline1);	
					$('#padds2').html(v.addsline2);	
					$('#pcity').html(v.city)	;
					$('#postalcode').html(v.postalcode);	
					$('#pcountry').html(v.country)	;
					$('#company').html(v.company)	;
					$('#designation').html(v.designation);	
					$('#cmobile').html(v.cmpnymobile)	;
					$('#ctelephone').html(v.cmpnytelephone);	
					$('#cemail').html(v.cmpnyemail)	;
					$('#cim').html(v.cmpnyim)	;
					$('#csm1').html('<a href="https://www.facebook.com/'+v.cmpnysocialmedia1+'" target="_blank">'+v.cmpnysocialmedia1+'</a>')	;
					$('#csm2').html('<a href="https://www.twitter.com/'+v.cmpnysocialmedia2+'"target="_blank">'+v.cmpnysocialmedia2)	;
					$('#cadds1').html(v.cmpnyaddsline1);	
					$('#cadds2').html(v.cmpnyaddsline2);	
					$('#ccity').html(v.cmpnycity);
					$('#zipcode').html(v.cmpnyzipcode);
					$('#ccountry').html(v.cmpnycountry);
					
				});

			});
			

		});

	});

</script>
<script type="text/javascript">
	$(document).ready(function() {
		
						$('#fdate').click(function() {
						//	alert("select date");
							$('#datepicker3').focus();
						});
						$('#tdate').click(function() {
							//	alert();
							$('#datepicker4').focus();
						});

						

						
					});
</script>

</head>
<body>
<!--
	 <!-- for edit 

	<div class="modal" id="edit" style="margin-top: 90px">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="panel-body">
					<div class="panel panel-card" style="margin-top: 20px;">
						<div class="panel-body">
							<button type="button" class="close" data-dismiss="modal">
								<b style="color: black">X</b>
							</button>
							<h4
								style="color: #d80204; margin-left: 30px; text-align: center; font-size: 20px">
								<b> Edit Availability</b>
							</h4>

							<div id="image"></div>
							<hr />
							<form class="form-horizontal group-border stripped"
								action="postavailability" method="post">

								<div class="form-group">
									<input type="hidden" name="action" value="update" /> <label
										class="col-lg-3 col-md-4 col-xs-12 control-label">Trip
										Id</label>
									<div class="col-lg-8 col-md-6">
										<input maxlength="200" name="tripid" id="tripid"
											required="required" class="form-control" readonly="readonly" />
									</div>
								</div>
								<br />
								<div class="form-group">
									<label
										class="col-lg-3 col-md-4 col-xs-12 col-sm-3 control-label">Trip</label>
									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
										<select class="form-control fancy-select2" name="pick"
											id="pick">

											<option value="" disabled="disabled" selected>From</option>

										</select>
									</div>

									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
										<select class="form-control fancy-select2" name="drop"
											id="drop">

											<option value="" disabled="disabled" selected>To</option>

										</select>
									</div>

								</div>
								<br />
								End .form-group 

								<div class="form-group">

									<label
										class="col-lg-3 col-md-4 col-xs-12 col-sm-3 control-label">Date</label>
									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
										<div class="input-group date">
											<input id="datepicker1" class="form-control" name="fromdate"
												style="text-align: center" type="text"
												onfocus="this.value = '';"
												onblur="if (this.value == '') {this.value = 'From Date';}"
												required> <span class="input-group-btn">
												<button type="button" class="btn btn-default">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
										<div class="input-group date">
											<input id="datepicker2" class="form-control" name="todate"
												style="text-align: center" type="text" value="To Date"
												onfocus="this.value = '';"
												onblur="if (this.value == '') {this.value = 'To Date';}"
												required="required"> <span class="input-group-btn">
												<button type="button" class="btn btn-default">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<br />
								End .form-group 
								<div class="form-group">
									<label
										class="col-lg-3 col-md-4 col-xs-12 col-sm-3 control-label">Vehicle
										Details</label>
									<div class="col-lg-4 col-md-3 col-xs-6 col-sm-4">
										<select class="form-control" name="vehicletype" id="vehicle"
											required="required">
											<option value="" disabled selected>Type of Vehicle</option>

										</select>
									</div>


									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
										<input maxlength="200" name="novehicles" id="nof"
											type="number" required="required" class="form-control"
											placeholder="No Of Vehicles" />
									</div>
								</div>
								<br />
								End .form-group 
								<div class="form-group">
									<label
										class="col-lg-3 col-md-4  col-xs-12 col-sm-3  control-label">Max
										Weight Limit </label>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<input maxlength="20" name="limit" id="weight" type="number"
											required="required" class="form-control"
											placeholder=" Weight" />
									</div>
								</div>
								<br />
								End .form-group 

								<div class="form-group">
									<div
										class="col-lg-offset-5 col-lg-1 col-md-offset-5 col-md-1 col-sm-offset-5 col-sm-1">
										<input type="submit" value="Update" class="btn r ">
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	modal close
 -->

	<nav class="navbar navbar-default ">
		<div class="container-fluid btn-primary">
			<div class="row">
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Contact Management System</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right btn-default">
						<li><a href="logout"><span
								class="glyphicon glyphicon-log-in"></span> Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
		<!-- modal ends here -->



	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading" style="display: flow-root">
				<a class="btn btn-primary" style="float: right"
					href="addcontact.jsp">Add New</a>
			</div>
			<div class="panel-body">

				<div class="panel panel-default">
					<div class="panel-body">
						<form action="search" method="post">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group" >
										<label>Name</label> <input type="text" name="name"
											placeholder="Enter name" class="form-control">

									</div>
									<div class="form-group">
										<label>Mobile</label> <input type="text" name="mobile"
											placeholder="Enter mobile" class="form-control">
									</div>
									<div class="form-group">
										<label>Country</label> <input type="text" name="country"
											placeholder="Enter country" class="form-control">
									</div>
								</div>
								<div class="col-md-4 form-group">
									<div class="form-group">
										<div class="col-md-12">
											<label>Group</label>
										</div>
 
										<div class="col-md-10">
											<select class="form-control" name="group" id="group" style="text-align: center;">
												<option disabled="disabled" selected="selected">Select Group</option>											 
											</select>
										</div>
									</div>
									<div class="form-group" >
										<div class="col-md-12 form-group" >
											<label>Category</label>
										</div>
										 
										<div class="col-md-10 form-group">
											<select class="form-control" name="category" id="category" style="text-align: center;">
												<option disabled="disabled" selected="selected">Select Category</option>

											</select>
											 

										</div>

									</div>
								</div>


								<div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<div class="input-group date">
											<input id="datepicker3" class="form-control" name="fdate"
												style="text-align: center" type="text"
												placeholder="From Date"> <span
												class="input-group-btn">
												<button id="fdate" type="button" class="btn btn-info ">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="form-group">
										<label>To date</label>
										<div class="input-group date">
											<input id="datepicker4" class="form-control" name="tdate"
												style="text-align: center" type="text" placeholder="To Date">
											<span class="input-group-btn">
												<button id="tdate" type="button" class="btn btn-info ">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-offset-4 col-md-7">
											<button class="btn btn-primary" type="submit">
											
												Search</button>
										</div>
									</div>
								</div>

							</div>
						</form>
						<!--row ends-->
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<table id="contactlist">
								<tr>
									<th>S.no</th>
									<th>Image</th>
									<th>Name</th>
									<th>Company</th>
									<th>Designation</th>
									<th>Mobile</th>
									<th>Email</th>

									<th>Action</th>

								</tr>
								<%
									Connection con12 = null;
									con12 = controller.dbcon.getConnection();
									ArrayList<contactpojo> al = new ArrayList<contactpojo>();

									try {

										PreparedStatement ps = con12.prepareStatement("SELECT * FROM `contacts` ORDER BY `id` DESC LIMIT 10 ");

										ResultSet rs = ps.executeQuery();
										int count = 1;
										//		out.println("<center>");
										while (rs.next()) {

											out.println("<tr>");
											String name = rs.getString("fname");
											out.print("<td style=\"display:none\">" + rs.getInt("id") + "</td>");
											out.print("<td>" + count + "</td>");
											out.print("<td><img width='100'  height='100' src='displayphoto?id=" + rs.getInt("id")
													+ "'></img></td>");
											out.print("<td>" + name + "</td>");
											out.print("<td>" + rs.getString("company") + "</td>");
											out.print("<td>" + rs.getString("designation") + "</td>");
											out.print("<td>" + rs.getString("mobile") + "</td>");
											out.print("<td>" + rs.getString("email") + "</td>");
											out.print( "<td><button id=\"editbtn\" class=\"btn btn-info\" ><i class=\"fa fa-pencil\"  ></i></button> <button class=\"btn btn-info\" id=\"view\"><i class=\"fa fa-eye\"  ></i></button> <button id=\"delete\" class=\"btn btn-info\" ><i class=\"fa fa-trash\"  ></i></button></td>");

											out.println("</tr>");

											count++;

										}
										//	out.println("</center>");
									} catch (Exception e) {
										e.printStackTrace();
									}

									/* 								 
																		int j;
																		ArrayList<contactpojo> al = (ArrayList<contactpojo>) request.getAttribute("contactpojo");
																		int count = 1;
									
																		for (j = 0; j < al.size(); j++) {
									
																			out.println("<center>");
																			out.println("<tr>");
																			contactpojo i = (contactpojo) al.get(j);
																			String name = i.getFirstname();
																			out.print("<td>" + count + "</td>");
																			out.print("<td><img width='100'  height='100' src='displayphoto?name=" + name + "'></img></td>");
																			out.print("<td>" + name + "</td>");
																			out.print("<td>" + i.getCompany() + "</td>");
																			out.print("<td>" + i.getDesignation() + "</td>");
																			out.print("<td>" + i.getMobile() + "</td>");
																			out.print("<td>" + i.getEmail() + "</td>");
																			out.print(
																					"<td><button class=\"btn btn-danger\" id=\"edit\">Edit</button> <button class=\"btn btn-info\" id=\"view\" data-toggle=\"modal\" data-target=\"#edit1\">View</button></td>");
									
																			out.println("</tr>");
																			count++;
																		} */
								%>



							</table>

						</div>
						<!--row ends-->
					</div>
				</div>

			</div>
		</div>
	</div>
	<!--panel ends here-->

		<!-- modal ends here -->

	<!-- for view -->
	<div class="modal" id="viewdetails" style="margin-top: 50px">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">
					</h3>
				</div>
				<div class="modal-body">
					<!--job Details Starts here-->
					<div class="panel panel-card ">
						<div class="contaoner-fluid">
							<div class="panel-body">


                                     <div class="col-md-12">
								
								<div class="row"
									style="margin-bottom: 5px; margin-top: -15px">

									<div class="posting-images">
										<div id="img1" class="col-md-3 ">
											<img class="img-rounded" width="150" height="100">
										</div>

									</div>
									<div class="posting-images">
										<div class="col-md-3 " id="img2">
											<img class="img-rounded" width="150" height="100">

										</div>
									</div>
									<div class="posting-images">
										<div id="img3" class="col-md-3 ">
											<img class="img-rounded" width="150" height="100">

										</div>
									</div>

									<div class="posting-images">
										<div id="img4" class="col-md-3">
											<img class="img-rounded" width="150" height="100">
										</div>
									</div>
								</div>
								</div>


                                 <div class="col-md-12">
									<div class="row">

									<div class="">
										<label class="col-md-2  control-label">Group</label>
										<div class="col-md-4 ">
											<span id="group1" style="margin-left: -40px;"> </span>
										</div>
									</div>
									<div class="">
										<label class="col-md-2 control-label">Ref Notes</label>
										<div class="col-md-3">
											<span id="refnotes"style="margin-left: -40px;"> </span>
										</div>
									</div>
								</div>
								</div>
								<div class="col-md-12">
									<div class="row">
									<div class="form-group">
										<label class=" col-md-2 control-label">Catogery</label>
										<div class="col-md-3">
											<span id="category1"style="margin-left: -40px;"> </span>
										</div>
									</div>
                                 </div>
								</div>
                                <div class="col-md-12" >
								<div class="panel panel-primary ">
									<div class="panel-heading">
										<div class="panel-title">Contact Info</div>

									</div>

									<div class="panel-body">
									<div class="col-md-12">
									<div class="row">
                                                    <div class="col-md-3">
                                                        <b>Title</b><span id="title1">:</span>
                                                    </div>
                                                     <div class="col-md-3">
                                                         <b>First Name</b><span id="fnam">:</span>
                                                    </div>
                                                     <div class="col-md-3">
                                                         <b>Last Name</b><span  id="lnam">:</span>
                                                    </div>
                                                     <div class="col-md-3">
                                                           <b>Nick Name</b><span  id="nickname">:</span>
                                                    </div>
												
												</div>
												<div class="row">
                                                    <div class="col-md-3 col-md-offset-3">
                                                       <b>Mobile</b><span id="mobnum">:</span></h5>
                                                    </div>
                                                     <div class="col-md-3">
                                                           <b>Telephone</b><span id="landline">:</span>
                                                    </div>
                                                     <div class="col-md-3">
                                                           <b>Email</b><span  id="emailid">:</span>
                                                    </div>
												
												</div>
									
										</div>
									</div>
								</div>
								</div>
							


								<div class="col-md-12">
									<div class="row">



										<div class="col-md-6">

											<div class="panel panel-primary">
												<div class="panel-heading">
													<div class="panel-title">Personal info</div>

												</div>
												<div class="panel-body">



													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Mobile</label>
															<div class="col-md-7 ">
																<span id="pmobile"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Telephone</label>
															<div class="col-md-7 ">
																<span id="ptelephone"></span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Email</label>
															<div class="col-md-7 ">
																<span id="pemail"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">IM</label>
															<div class="col-md-7 ">
																<span id="pim"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Social
																Media 1</label>
															<div class="col-md-7 ">
																<span id="psm1"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Social
																Media 2</label>
															<div class="col-md-7 ">
																<span id="psm2"></span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Address
																Line 1</label>
															<div class="col-md-7 ">
																<span id="padds1"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Address
																Line 2</label>
															<div class="col-md-7 ">
																<span id="padds2"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">City</label>
															<div class="col-md-7 ">
																<span id="pcity"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Post
																Code</label>
															<div class="col-md-7 ">
																<span id="postalcode"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group" style="height: 60px;">
															<label class="col-md-5  control-label">Country</label>
															<div class="col-md-7 ">
																<span id="pcountry"> </span>
															</div>
														</div>
													</div>



												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="panel panel-primary">
												<div class="panel-heading ">
													<div class="panel-title">Company</div>

												</div>
												<div class="panel-body">

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Company</label>
															<div class="col-md-7 ">
																<span id="company"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Designation/Dept</label>
															<div class="col-md-7 ">
																<span id="designation"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Mobile</label>
															<div class="col-md-7 ">
																<span id="cmobile"></span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Telephone</label>
															<div class="col-md-7 ">
																<span id="ctelephone"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Email</label>
															<div class="col-md-7 ">
																<span id="cemail"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">IM</label>
															<div class="col-md-7 ">
																<span id="cim"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Social
																Media 1</label>
															<div class="col-md-7 ">
																<span id="csm1"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Social
																Media 2</label>
															<div class="col-md-7 ">
																<span id="csm2"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Address
																Line 1</label>
															<div class="col-md-7 ">
																<span id="cadds1"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Address
																Line 2</label>
															<div class="col-md-7 ">
																<span id="cadds2"> </span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">City</label>
															<div class="col-md-7 ">
																<span id="ccity"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Post Code</label>
															<div class="col-md-7 ">
																<span id="zipcode"> </span>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<label class="col-md-5  control-label">Country</label>
															<div class="col-md-7 ">
																<span id="ccountry"> </span>
															</div>
														</div>
													</div>


												</div>
											</div>
										</div>
									</div>
								</div>



							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- for view  -->

</body>
</html>

