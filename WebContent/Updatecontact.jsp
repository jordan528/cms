<%@page import="java.util.ArrayList"%>
<%@page import="pojo.contactpojo"%>
<%@page language="java" import="java.util.*"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%
	ResultSet resultset = null;
if(session.getAttribute("admin")!="admin"){
	response.sendRedirect("index.jsp");
}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<title>Update Contact</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/panel.css" />
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 -->
<% int sno= Integer.parseInt(request.getParameter("sno")); %>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	$(document).ready( function() {
				 
				
		$('#img1').html( "<img width='150'  height='150' src='displayphoto?id=" + <%=sno%> + "'></img>");
		$('#img2').html( "<img width='150'  height='150' src='img2servlet?id=" + <%=sno%>+ "'></img>");
		$('#img3').html( "<img width='150'  height='150' src='img3servlet?id=" + <%=sno%>+ "'></img>");
		$('#img4').html( "<img width='150'  height='150' src='img4servlet?id=" + <%=sno%>+ "'></img>");


			});
</script>
<style>

/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 10;
	border-radius: 0;
	iv
	/
}

/* Add a gray background color and some padding to the footer */
footer {
	background-color: #f2f2f2;
	padding: 25px;
}
</style>
</head>
<body>
<nav class="navbar navbar-default ">
		<div class="container-fluid btn-primary">
			<div class="row">
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="landingpage.jsp">Contact Management System</a></li>

					</ul>
					<ul class="nav navbar-nav navbar-right btn-">
						<!-- <li><a href="logout"><span
								class="glyphicon glyphicon-log-in"></span> Logout</a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</nav>

	<jsp:useBean id="connn" class="controller.dbcon" scope="page"></jsp:useBean>
	<%
		try {

			Connection conn = connn.getConnection();

			PreparedStatement ps = conn.prepareStatement("select *from contacts where id=?");
			ps.setInt(1, sno);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
	%>


	<form action="updatecontact"  method="post">
		<input type="hidden" name="sno1"  value="<%=request.getParameter("sno")%>">
		
		<div class="container-fluid" style="margin-top: 1px">

			<div class=" panel panel-primary">
				<div class="panel-heading bg-info">
					<div class="panel-title">Edit Contact</div>
					<div
						style="float: right; font-size: 85%; position: relative; top: -28px">
						<a class="btn btn-default" type="button" href="landingpage.jsp">Cancel</a>
						<button id="signinlink" class="btn btn-success" type="submit">Update</button>

					</div>

				</div>


				<div class="panel-body ">
						<div class="row col-md-12 center" style="margin-bottom: 15px;margin-left:60px;">

									<div class="">
										<div id="img1" class="col-md-3 ">
											<img class="img-rounded" width="150" height="100">
										</div>

									</div>
									<div class="">
										<div class="col-md-3 " id="img2">
											<img class="img-rounded" width="150" height="100">

										</div>
									</div>
									<div class="">
										<div id="img3" class="col-md-3 ">
											<img class="img-rounded" width="150" height="100">

										</div>
									</div>

									<div class="">
										<div id="img4" class="col-md-3">
											<img class="img-rounded" width="150" height="100">
										</div>
									</div>
								</div>
					<div class="row">
						<div class="form-group">
							<label class="col-md-1 control-label">Group</label>
							<jsp:useBean id="con1" class="controller.dbcon" scope="page"></jsp:useBean>
							<%
								try {
											//Class.forName("com.mysql.jdbc.Driver").newInstance();
											/* 
											DriverManager.registerDriver(new com.mysql.jdbc.Driver());	
											Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/cms","root",""); */
											//	Connection conn = con1.getConnection();
											Statement statement = conn.createStatement();

											resultset = statement.executeQuery("select * from groups");
							%>
							<div class="col-md-5 ">



								<select name="group" id="dropdown" class="form-control" required>
									<option selected="selected"><%=rs.getString("groups")%></option>
									<%
										while (resultset.next()) {
									%>
									<option><%=resultset.getString(2)%></option>
									<%
										}
									%>

								</select>
								<%
									} catch (Exception e) {
												out.println("wrong entry" + e);
											}
								%>

							</div>

							<label class=" col-md-1 control-label">Ref Notes</label>
							<div class=" col-md-5">
								<textarea name="refnotes" id="refNotes" class="form-control"
									placeholder="" required><%=rs.getString("refnotes")%></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-1 control-label">catogary</label>
							<jsp:useBean id="con" class="controller.dbcon" scope="page"></jsp:useBean>
							<%
								try {
											//Class.forName("com.mysql.jdbc.Driver").newInstance();
											/* 
											DriverManager.registerDriver(new com.mysql.jdbc.Driver());	
											Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/cms","root",""); */
											//		Connection conn = con.getConnection();
											Statement statement = conn.createStatement();

											resultset = statement.executeQuery("select * from category");
							%>

							<div class="col-md-5">
								<select name="catogary" id="companytype" class="form-control"
									required>

									<%
										while (resultset.next()) {
									%>
									<option><%=resultset.getString(2)%></option>
									<%
										}
									%>

								</select>
								<%
									} catch (Exception e) {
												out.println("wrong entry" + e);
											}
								%>


							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-primary " style="margin-left:18px;margin-right:18px;">
					<div class="panel-heading">
						<div class="panel-title">Contact Info</div>

					</div>

					<div class="panel-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-5">
									<div id="div_id_catagory">
										<label for="title" class="control-label col-md-1">
											Title</label>
										<div class="controls col-md-3 ">
											<input class="input-md textinput textInput form-control"
												id="title" name="title" value="<%=rs.getString("title")%>"
												type="text" />
										</div>
									</div>
									<div id="div_id_fname">
										<label for="first_name" class="control-label col-md-3">First
											Name</label>
										<div class="controls col-md-5 ">
											<input class="input-md textinput textInput form-control"
												id="first_name" name="fname"
												value="<%=rs.getString("fname")%>" type="text" />
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div id="div_id_lname">
										<label for="lastname" class="control-label col-md-2">Last
											Name</label>
										<div class="controls col-md-4">
											<input class="input-md textinput textInput form-control"
												id="lastname" name="lname"
												value="<%=rs.getString("lname")%>" type="text" />
										</div>
									</div>
									<div id="div_id_nname">
										<label for="nickname" class="control-label col-md-2">Nick
											Name</label>
										<div class="controls col-md-4">
											<input class="input-md textinput textInput form-control"
												id="" nickname"" name="nname"
												value="<%=rs.getString("nname")%>" type="text" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-5">

									<div id="div_id_number">
										<label for="lastname"
											class="control-label col-md-3  col-md-offset-4">Mobile</label>
										<div class="controls col-md-5">
											<input class="input-md textinput textInput form-control"
												id="mobile" name="mobile"
												value="<%=rs.getString("mobile")%>" type="text" />
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div id="div_id_number">
										<label for="nickname" class="control-label col-md-2">Telephone</label>
										<div class="controls col-md-4">
											<input class="input-md textinput textInput form-control"
												id="tetephone" name="telephone"
												value="<%=rs.getString("telephone")%>" type="text" />
										</div>
									</div>
									<div id="div_id_number">
										<label for="nickname" class="control-label col-md-2">Email</label>
										<div class="controls col-md-4">
											<input class="input-md textinput textInput form-control"
												id="email" name="email" value="<%=rs.getString("email")%>"
												type="email" />
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>



				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6">

							<div class="panel panel-primary" style="padding-bottom: 110px;">
								<div class="panel-heading">
									<div class="panel-title">Personal info</div>

								</div>
								<div class="panel-body">

									<div id="mobile" class="form-group ">
										<label for="mobile" class="control-label col-md-4 ">
											Mobile</label>
										<div class="controls col-md-8 ">
											<input class="input-md  textinput textInput form-control"
												id="id_username" maxlength="30" name="pmobile"
												value="<%=rs.getString("pmobile")%>"
												style="margin-bottom: 10px" type="phone" />
										</div>
									</div>
									<div id="telephone" class="form-group ">
										<label for="telephone"
											class="control-label col-md-4  requiredField">
											Telephone </label>
										<div class="controls col-md-8 ">
											<input class="input-md emailinput form-control" id="id_email"
												name="ptelephone" value="<%=rs.getString("ptelephone")%>"
												style="margin-bottom: 10px" type="phone" />
										</div>
									</div>
									<div id="email" class="form-group ">
										<label for="email"
											class="control-label col-md-4  requiredField">Emai</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="email" name="pemail" value="<%=rs.getString("pemail")%>"
												style="margin-bottom: 10px" type="email" />
										</div>
									</div>
									<div id="im" class="form-group ">
										<label for="im" class="control-label col-md-4  requiredField">
											IM </label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="im" name="pim" value="<%=rs.getString("pim")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="" class="form-group ">
										<label for="" class="control-label col-md-4  requiredField">
											Social Media 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="social_media1" name="socialmedia1"
												value="<%=rs.getString("socialmedia1")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

									<div id="div_id_company" class="form-group required">
										<label for="id_company"
											class="control-label col-md-4  requiredField"> Social
											Media 2 </label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_company" name="socialmedia2"
												value="<%=rs.getString("socialmedia2")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_catagory" class="form-group required">
										<label for="id_catagory"
											class="control-label col-md-4  requiredField">
											Address Line 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_catagory" name="addsline1"
												value="<%=rs.getString("addsline1")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Address Line 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="addsline2"
												value="<%=rs.getString("addsline2")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField"> City</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="pcity"
												value="<%=rs.getString("pcity")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField"> Postal
											Code</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="postalcode"
												value="<%=rs.getString("potalcode")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Country</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="pcountry"
												value="<%=rs.getString("pcountry")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>



								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-primary">
								<div class="panel-heading ">
									<div class="panel-title">Company</div>

								</div>
								<div class="panel-body">

									<div id="div_id_username" class="form-group required">
										<label for="id_username"
											class="control-label col-md-4  requiredField">
											Company</label>
										<div class="controls col-md-8 ">
											<input class="input-md  textinput textInput form-control"
												id="id_username" maxlength="30" name="company"
												value="<%=rs.getString("company")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_email" class="form-group required">
										<label for="id_email"
											class="control-label col-md-4  requiredField">Designation
											/ Dept</label>
										<div class="controls col-md-8 ">
											<input class="input-md emailinput form-control" id="id_email"
												name="desgnation" value="<%=rs.getString("designation")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_catagory" class="form-group required">
										<label for="id_catagory"
											class="control-label col-md-4  requiredField"> Mobile</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_catagory" name="cmobile"
												value="<%=rs.getString("cmobile")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_number" class="form-group required">
										<label for="id_number"
											class="control-label col-md-4  requiredField">
											Telephone</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_number" name="ctelephone"
												value="<%=rs.getString("ctelephone")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> Email</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="cemail"
												value="<%=rs.getString("cemail")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">Im</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="cim" value="<%=rs.getString("cim")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> Social
											Media 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="csocialmedia1"
												value="<%=rs.getString("csocialmedia1")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">Social
											Media 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="csocialmedia2"
												value="<%=rs.getString("csocialmedia2")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											Address Line 1</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="caddsline1"
												value="<%=rs.getString("caddsline1")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											Address Line 2</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="caddsline2"
												value="<%=rs.getString("caddsline2")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField"> City</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="ccity"
												value="<%=rs.getString("ccity")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">
											Zipcode</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="zipcode"
												value="<%=rs.getString("zipcode")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>
									<div id="div_id_location" class="form-group required">
										<label for="id_location"
											class="control-label col-md-4  requiredField">Country</label>
										<div class="controls col-md-8 ">
											<input class="input-md textinput textInput form-control"
												id="id_location" name="ccountry"
												value="<%=rs.getString("ccountry")%>"
												style="margin-bottom: 10px" type="text" />
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</form>
	<%
		}
		} catch (Exception e) {
			out.println("wrong entry" + e);
		}
	%>
</body>
</html>