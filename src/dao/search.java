package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojo.contactpojo;

public class search {

	String qur = "select * from  contacts where ", sub = "", c_name = "", c_mobile = "", cntry = "", group = "",
			category = "", from = "", to = "";

	public List<pojo.contactpojo> getContacts(String name, String mobile, String country, String grp, String cat,
			String fdate, String todate) throws Exception {
		// System.out.println(name + mobile + grp + cat + fdate + todate);
		Connection con = null;
		con = controller.dbcon.getConnection();
		ArrayList<contactpojo> al = new ArrayList<contactpojo>();

		try {

			if (!name.equalsIgnoreCase("") && name != null) {
				c_name = name;
				sub += "fname like " + "'"+c_name + "%"+"'";
				System.out.println(qur + sub);
			}
			if (mobile != null && !mobile.equalsIgnoreCase("")) {
				c_mobile = mobile;
				if (sub.equalsIgnoreCase("")) {
					sub += "mobile like " + "'"+ c_mobile + "%"+"'";
				} else {
					sub += " and mobile like " + "'"+ c_mobile + "%"+"'";
				}
				System.out.println(qur + sub);
			}
			if (country != null && !country.equalsIgnoreCase("")) {
				cntry = country;
				if (sub.equalsIgnoreCase("")) {
					sub += "country like " + "'"+ cntry + "%"+"'";
				} else {
					sub += " and country like " + "'"+ cntry +"%"+"'";
				}
				System.out.println(qur + sub);
			}
			if (grp != null && !grp.equalsIgnoreCase("")) {
				if (sub.equalsIgnoreCase("")) {
					sub += "groups = " +  "'"+grp+ "'";
				} else {
					sub += " and groups = " + "'"+ grp+ "'";
				}
				System.out.println(qur + sub);
			}
			if (cat != null && !cat.equalsIgnoreCase("")) {
				if (sub.equalsIgnoreCase("")) {
					sub += "catogary =" +  "'"+cat+ "'";
				} else {
					sub += " and catogary =" +  "'"+cat +"'";
				}
				System.out.println(qur + sub);
			}
			if (fdate != null && !fdate.equalsIgnoreCase("") && todate != null && !todate.equalsIgnoreCase("")) {
				if (sub.equalsIgnoreCase("")) {
					sub += "`TimeStamp` BETWEEN "+ "'"+ fdate+  "'"+" and "+  "'"+todate+ "'";
				} else {
					sub += " and `TimeStamp` BETWEEN "+  "'"+fdate+ "'"+ " and "+ "'"+ todate+ "'";
				}
				System.out.println(qur + sub);
			
			}
			qur +=sub;
			
			System.out.println("final query"+qur);

			PreparedStatement ps = con.prepareStatement(qur);
				//	"SELECT * FROM `contacts` where (fname like ? and ( mobile like ? or groups=? or catogary=? or TimeStamp between ? and ?))");
		/*	ps.setString(1, "%" + name + "%");
			ps.setString(2, "%" + mobile + "%");
			ps.setString(3, grp);
			ps.setString(4, cat);
			ps.setString(5, fdate);
			ps.setString(6, todate);
*/
			ResultSet rs = ps.executeQuery();
			/*
			 * if(rs.next()) { System.out.println(rs.getInt("id")); }
			 * System.out.println(rs);
			 */
			while (rs.next()) {

				contactpojo bean = new contactpojo();
				bean.setSno(rs.getInt("id"));
				bean.setImg1(rs.getBlob("img1"));
				bean.setImg2(rs.getBlob("img2"));
				bean.setImg3(rs.getBlob("img3"));
				bean.setImg4(rs.getBlob("img4"));
				bean.setTitle(rs.getString("title"));
				bean.setFirstname(rs.getString("fname"));
				bean.setNickname(rs.getString("nname"));
				bean.setLastname(rs.getString("lname"));
				bean.setMobile(rs.getString("mobile"));
				bean.setTelephone(rs.getString("telephone"));
				bean.setEmail(rs.getString("email"));
				bean.setPrsnlmoble(rs.getString("pmobile"));
				bean.setPrsnltelephone(rs.getString("ptelephone"));
				bean.setPrsnlemail(rs.getString("pemail"));
				bean.setPrsnlim(rs.getString("pim"));
				bean.setSocialmedia1(rs.getString("socialmedia1"));
				bean.setSocialmedia2(rs.getString("socialmedia2"));
				bean.setAddsline1(rs.getString("addsline1"));
				bean.setAddsline2(rs.getString("addsline2"));
				bean.setCity(rs.getString("pcity"));
				bean.setPostalcode(rs.getString("potalcode"));
				bean.setCountry(rs.getString("pcountry"));

				bean.setCompany(rs.getString("company"));
				bean.setDesignation(rs.getString("designation"));
				bean.setCmpnymobile(rs.getString("pmobile"));
				bean.setCmpnytelephone(rs.getString("ctelephone"));
				bean.setCmpnyemail(rs.getString("cemail"));
				bean.setCmpnyim(rs.getString("cim"));
				bean.setCmpnysocialmedia1(rs.getString("csocialmedia1"));
				bean.setCmpnysocialmedia2(rs.getString("csocialmedia2"));
				bean.setCmpnyaddsline1(rs.getString("caddsline1"));
				bean.setCmpnyaddsline2(rs.getString("caddsline2"));
				bean.setCmpnycity(rs.getString("ccity"));
				bean.setCmpnyzipcode(rs.getString("zipcode"));
				bean.setCmpnycountry(rs.getString("ccountry"));

				al.add(bean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;

	}

	public List<pojo.contactpojo> getRecentContacts() throws Exception {
		Connection con = null;
		con = controller.dbcon.getConnection();
		ArrayList<contactpojo> al = new ArrayList<contactpojo>();

		try {

			PreparedStatement ps = con.prepareStatement("SELECT * FROM `contacts` ORDER BY `id` DESC LIMIT 10 ");

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				contactpojo bean = new contactpojo();
				bean.setSno(rs.getInt("id"));
				bean.setImg1(rs.getBlob("img1"));
				bean.setImg2(rs.getBlob("img2"));
				bean.setImg3(rs.getBlob("img3"));
				bean.setImg4(rs.getBlob("img4"));
				bean.setTitle(rs.getString("title"));
				bean.setFirstname(rs.getString("fname"));
				bean.setNickname(rs.getString("nname"));
				bean.setLastname(rs.getString("lname"));
				bean.setMobile(rs.getString("mobile"));
				bean.setTelephone(rs.getString("telephone"));
				bean.setEmail(rs.getString("email"));
				bean.setPrsnlmoble(rs.getString("pmobile"));
				bean.setPrsnltelephone(rs.getString("ptelephone"));
				bean.setPrsnlemail(rs.getString("pemail"));
				bean.setPrsnlim(rs.getString("pim"));
				bean.setSocialmedia1(rs.getString("socialmedia1"));
				bean.setSocialmedia2(rs.getString("socialmedia2"));
				bean.setAddsline1(rs.getString("addsline1"));
				bean.setAddsline2(rs.getString("addsline2"));
				bean.setCity(rs.getString("pcity"));
				bean.setPostalcode(rs.getString("potalcode"));
				bean.setCountry(rs.getString("pcountry"));

				bean.setCompany(rs.getString("company"));
				bean.setDesignation(rs.getString("designation"));
				bean.setCmpnymobile(rs.getString("pmobile"));
				bean.setCmpnytelephone(rs.getString("ctelephone"));
				bean.setCmpnyemail(rs.getString("cemail"));
				bean.setCmpnyim(rs.getString("cim"));
				bean.setCmpnysocialmedia1(rs.getString("csocialmedia1"));
				bean.setCmpnysocialmedia2(rs.getString("csocialmedia2"));
				bean.setCmpnyaddsline1(rs.getString("caddsline1"));
				bean.setCmpnyaddsline2(rs.getString("caddsline2"));
				bean.setCmpnycity(rs.getString("ccity"));
				bean.setCmpnyzipcode(rs.getString("zipcode"));
				bean.setCmpnycountry(rs.getString("ccountry"));
				al.add(bean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;

	}

}
