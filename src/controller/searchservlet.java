package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.search;
import pojo.contactpojo;

/**
 * Servlet implementation class search
 */
@WebServlet("/search")
public class searchservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		String country = request.getParameter("country");
		String grp = request.getParameter("group");
		String cat = request.getParameter("category");
		String fdate = request.getParameter("fdate");
		String todate = request.getParameter("tdate");

		response.setContentType("text/html");

		search dao = new search();
		try {

				List<contactpojo> al = dao.getContacts(name, mobile, country, grp, cat, fdate, todate);
				request.setAttribute("contactpojo", al);
				request.setAttribute("searchcriteria",
						"{ Name: " + name + ",Mobile: " + mobile + ",Group: " + grp + ",Category " + cat + ",Country: "
								+ country + ",From Date: " + fdate + ",To Date: " + todate + " }");
				RequestDispatcher rd = request.getRequestDispatcher("search.jsp");
				rd.forward(request, response);

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
