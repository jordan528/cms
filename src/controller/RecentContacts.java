package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.search;
import pojo.contactpojo;
import pojo.groupbean;

/**
 * Servlet implementation class RecentContacts
 */
@WebServlet("/recentcontacts")
public class RecentContacts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		response.setContentType("text/html");

		search dao = new search();
		try {
			List<contactpojo> al = dao.getRecentContacts();
			request.setAttribute("contactpojo", al);

			RequestDispatcher rd = request.getRequestDispatcher("landingpage.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}

}
