package controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Img3servlet
 */
@WebServlet("/img3servlet")
public class Img3servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Img3servlet() {
        super();
    }

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String id=request.getParameter("id");

			Connection con=null;
		  		
		  		try {
					con=dbcon.getConnection();
					PreparedStatement ps=con.prepareStatement("select img3 from contacts where id=?");
					ps.setString(1, id);
					ResultSet rs=ps.executeQuery();
					rs.next();
					Blob b=rs.getBlob("img3");
					response.setContentType("image/jpeg");
					
					if(b.length() !=0){
						response.setContentLength((int) b.length());
						InputStream is= b.getBinaryStream();
						OutputStream os=response.getOutputStream();
						byte buff[]=new byte[(int) b.length()];
						is.read(buff);
						os.write(buff);
						os.close();
						}
						else{
						
					//	response.setContentType("image/jpeg");  
					    ServletOutputStream out;  
					    out = response.getOutputStream();  
					    FileInputStream fin = new FileInputStream(getServletContext().getRealPath("images/default.jpg"));  
					      
					    BufferedInputStream bin = new BufferedInputStream(fin);  
					    BufferedOutputStream bout = new BufferedOutputStream(out);  
					    int ch =0; ;  
					    while((ch=bin.read())!=-1)  
					    {  
					    bout.write(ch);  
					    }  
					      
					    bin.close();  
					    fin.close();  
					    bout.close();  
					    out.close();  
						}
					/*response.setContentLength((int) b.length());
					InputStream is= b.getBinaryStream();
					OutputStream os=response.getOutputStream();
					byte buff[]=new byte[(int) b.length()];
					is.read(buff);
					os.write(buff);
					os.close();
				*/
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
		  		


		
		}

}
