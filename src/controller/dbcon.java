package controller;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class dbcon {
	
	public static void clean(Connection con, PreparedStatement ps)
	 {
	   try
	   { if ( ps != null )  ps.close();
	     if ( con != null) con.close();
	   }
	    catch(Exception ex)
	   { 
	    	System.out.println(ex.getMessage()); }
	 }
	
	 public static Connection  getConnection() throws Exception
	 {
		DriverManager.registerDriver(new com.mysql.jdbc.Driver());	
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/cms","root","root");
	    return con;
	 }
	 
	 public static void main(String []args) throws Exception{
		 
		 Connection co=  getConnection();
		 if(co!= null){
			 System.out.println("connected");
		 }
	 }
	

}
