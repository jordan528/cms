package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddGroup
 */
@WebServlet("/AddGroup")
public class AddGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddGroup() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		String grpname = request.getParameter("grpname");

		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = controller.dbcon.getConnection();

			ps = con.prepareStatement("INSERT INTO `groups`(`id`, `grpname`) VALUES (?,?)");
			ps.setInt(1, 0);
			ps.setString(2, grpname);

			int rs = ps.executeUpdate();
			System.out.println(rs);
			if (rs > 0) {
				// request.getRequestDispatcher("index.jsp").forward(request,
				// response);
			//	response.sendRedirect("addcontact.jsp");
				response.getWriter().write("Group Added Successfully");
			}else{
				//	response.sendRedirect("addcontact.jsp");
				response.getWriter().write("Failed..Try again..");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
