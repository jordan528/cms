package controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet implementation class UpdateContact
 */
@WebServlet("/updatecontact")
public class UpdateContact extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateContact() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		/*
		 * Part img1 = request.getPart("img1"); Part img2 =
		 * request.getPart("img2"); Part img3 = request.getPart("img3"); Part
		 * img4 = request.getPart("img4");
		 */
		//System.out.println(request.getParameter("sno1"));
		int id = Integer.parseInt(request.getParameter("sno1"));

		String group = request.getParameter("group");
		String catogary = request.getParameter("catogary");
		String refnotes = request.getParameter("refnotes");
		String title = request.getParameter("title");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String nname = request.getParameter("nname");
		String mobile = request.getParameter("mobile");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");

		// personal data
		String pmobile = request.getParameter("pmobile");
		String ptelephone = request.getParameter("ptelephone");
		String pemail = request.getParameter("pemail");
		String pim = request.getParameter("pim");
		String social1 = request.getParameter("socialmedia1");
		String social2 = request.getParameter("socialmedia2");
		String addsline1 = request.getParameter("addsline1");
		String addsline2 = request.getParameter("addsline2");
		String pcity = request.getParameter("pcity");
		String postalcode = request.getParameter("postalcode");
		String pcountry = request.getParameter("pcountry");

		// company data
		String company = request.getParameter("company");
		String desgnation = request.getParameter("desgnation");
		String cmobile = request.getParameter("cmobile");
		String ctelephone = request.getParameter("ctelephone");
		String cemail = request.getParameter("cemail");
		String cim = request.getParameter("cim");
		String csm1 = request.getParameter("csocialmedia1");
		String csm2 = request.getParameter("csocialmedia2");
		String caddsline1 = request.getParameter("caddsline1");
		String caddsline2 = request.getParameter("caddsline2");
		String ccity = request.getParameter("ccity");
		String zipcode = request.getParameter("zipcode");
		String ccountry = request.getParameter("ccountry");

		/*
		 * InputStream is= img1.getInputStream(); InputStream is1=
		 * img2.getInputStream(); InputStream is2= img3.getInputStream();
		 * InputStream is3= img4.getInputStream();
		 * 
		 * System.out.println(img1+":-:"+img1.getSize()+":--:"+is);
		 */
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = controller.dbcon.getConnection();
			ps = con.prepareStatement(
					"UPDATE `contacts` SET groups=?,`catogary`=?,`refnotes`=?,`title`=?,`fname`=?,`lname`=?,`nname`=?,`mobile`=?,`telephone`=?,`email`=?,`pmobile`=?,`ptelephone`=?,`pemail`=?,`pim`=?,`socialmedia1`=?,`socialmedia2`=?,`addsline1`=?,`addsline2`=?,`pcity`=?,`potalcode`=?,`pcountry`=?,`company`=?,`designation`=?,`cmobile`=?,`ctelephone`=?,`cemail`=?,`cim`=?,`csocialmedia1`=?,`csocialmedia2`=?,`caddsline1`=?,`caddsline2`=?,`ccity`=?,`zipcode`=?,`ccountry`=? WHERE `id`=?");

			ps.setString(1, group);
			ps.setString(2, catogary);
			ps.setString(3, refnotes);
			ps.setString(4, title);
			ps.setString(5, fname);
			ps.setString(6, lname);
			ps.setString(7, nname);
			ps.setString(8, mobile);
			ps.setString(9, telephone);
			ps.setString(10, email);
			ps.setString(11, pmobile);
			ps.setString(12, ptelephone);
			ps.setString(13, pemail);
			ps.setString(14, pim);
			ps.setString(15, social1);
			ps.setString(16, social2);
			ps.setString(17, addsline1);
			ps.setString(18, addsline2);
			ps.setString(19, pcity);
			ps.setString(20, postalcode);
			ps.setString(21, pcountry);
			ps.setString(22, company);
			ps.setString(23, desgnation);
			ps.setString(24, cmobile);
			ps.setString(25, ctelephone);
			ps.setString(26, cemail);
			ps.setString(27, cim);
			ps.setString(28, csm1);
			ps.setString(29, csm2);
			ps.setString(30, caddsline1);
			ps.setString(31, caddsline2);
			ps.setString(32, ccity);
			ps.setString(33, zipcode);
			ps.setString(34, ccountry);
			ps.setInt(35, id);
			int rs = ps.executeUpdate();
			System.out.println(rs + id);
			if (rs > 0) {
				response.sendRedirect("landingpage.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
