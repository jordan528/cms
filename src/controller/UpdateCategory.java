package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateCategory
 */
@WebServlet("/updatecategory")
public class UpdateCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			response.setContentType("text/html");

			String oldcatname = request.getParameter("oldcatname");
			String newcatname = request.getParameter("newcatname");
			Connection con = null;
			PreparedStatement ps = null;
//			System.out.println("updating grp"+oldcatname+"new frpname"+newcatname);

				try {
					con = controller.dbcon.getConnection();
						ps = con.prepareStatement("UPDATE `category` SET `categoryname`=? WHERE `categoryname`=?");
						ps.setString(1, newcatname);
						ps.setString(2, oldcatname);
						int rs = ps.executeUpdate();
						if (rs > 0) {
							
							ps=con.prepareStatement("UPDATE contacts SET catogary=? where catogary=?");
							ps.setString(1, newcatname);
							ps.setString(2, oldcatname);
							ps.executeUpdate();
						//	response.sendRedirect("addcontact.jsp");
							response.getWriter().write("Category Name Updated Successfully");
						}else{
						//.sendRedirect("addcontact.jsp");
							response.getWriter().write("Failed..Try again..");
						}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
	
	}

		
		
		
		}

