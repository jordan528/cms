package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import pojo.contactpojo;

/**
 * Servlet implementation class Getdetails
 */
@WebServlet("/getdetails")
public class Getdetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Getdetails() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection con = null;
		PreparedStatement ps = null;
		String name = request.getParameter("id");

		ArrayList<contactpojo> al = new ArrayList<contactpojo>();
		try {
			con = dbcon.getConnection();

			ps = con.prepareStatement("select * from contacts where id=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				contactpojo bean = new contactpojo();
				bean.setTitle("title");
				bean.setGrpname(rs.getString("groups"));
				bean.setCatname(rs.getString("catogary"));
				bean.setRefnotes(rs.getString("refnotes"));
				bean.setFirstname(rs.getString("fname"));
				bean.setLastname(rs.getString("lname"));
				bean.setNickname(rs.getString("nname"));
				bean.setMobile(rs.getString("mobile"));
				bean.setTelephone(rs.getString("telephone"));
				bean.setEmail(rs.getString("email"));
				bean.setPrsnlmoble(rs.getString("pmobile"));
				bean.setPrsnltelephone(rs.getString("ptelephone"));
				bean.setPrsnlemail(rs.getString("pemail"));
				bean.setPrsnlim(rs.getString("pim"));
				bean.setSocialmedia1(rs.getString("socialmedia1"));
				bean.setSocialmedia2(rs.getString("socialmedia2"));
				bean.setAddsline1(rs.getString("addsline1"));
				bean.setAddsline2(rs.getString("addsline2"));
				bean.setCity(rs.getString("pcity"));
				bean.setPostalcode(rs.getString("potalcode"));
				bean.setCountry(rs.getString("pcountry"));
				bean.setCompany(rs.getString("company"));
				bean.setDesignation(rs.getString("designation"));
				bean.setCmpnymobile(rs.getString("cmobile"));
				bean.setCmpnytelephone(rs.getString("ctelephone"));
				bean.setCmpnyemail(rs.getString("cemail"));
				bean.setCmpnyim(rs.getString("cim"));
				bean.setCmpnysocialmedia1(rs.getString("csocialmedia1"));
				bean.setCmpnysocialmedia2(rs.getString("csocialmedia2"));
				bean.setCmpnyaddsline1(rs.getString("caddsline1"));
				bean.setCmpnyaddsline2(rs.getString("caddsline2"));
				bean.setCmpnycity(rs.getString("ccity"));
				bean.setCmpnyzipcode(rs.getString("zipcode"));
				bean.setCmpnycountry(rs.getString("ccountry"));

				al.add(bean);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Gson g = new Gson();

		String json = g.toJson(al);
		response.setContentType("application/json");
		response.getWriter().write(json);
	//	System.out.println(json);

	}

}
