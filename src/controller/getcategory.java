package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import pojo.groupbean;

/**
 * Servlet implementation class getcategory
 */
@WebServlet("/getcategory")
public class getcategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getcategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = null;
		PreparedStatement ps = 	null;
		ArrayList<groupbean> al = new ArrayList<groupbean>();
		try {
			con=dbcon.getConnection();

			ps = con.prepareStatement("select * from category");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				groupbean bean = new groupbean();
				bean.setCatname(rs.getString("categoryname"));

				al.add(bean);
			//	request.setAttribute("groups", al);

			}

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson g= new Gson();
		JsonElement je=g.toJsonTree(al,new TypeToken<List<groupbean>>() {}.getType());
		JsonArray json=je.getAsJsonArray();
		response.setContentType("application/json");
		response.getWriter().print(json);

	}

}
