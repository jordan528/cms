package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userdata;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uname = request.getParameter("username");
		String psw = request.getParameter("password");

		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		boolean b = false;
		userdata ud = new userdata();
		try {
			b = ud.checkLogin(uname, psw);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpSession s = request.getSession();

		if (b) {
			s.setAttribute("admin", "admin");
			response.sendRedirect("landingpage.jsp");

		} else {

			request.getRequestDispatcher("index.jsp").include(request, response);
    		pw.println("<script type=\"text/javascript\" src=\"assets/jquery-3.1.1.min.js\"></script>");
			pw.println("<script src=\"assets/sweetalert.min.js\"></script>");
			pw.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"assets/sweetalert.css\">");
    		
    		pw.println("<script type=\"text/javascript\">");
			pw.println("	$(document).ready(function() {");
    
			pw.println("swal({ title: \"Invalid User!\",text: \"UserName/Password incorrect!!!\", type:\"error\", confirmButtonColor: \"#d80204\",confirmButtonText: \"OK\",closeOnConfirm: true},function(){  window.location.href = \"index.jsp\"; }); });");

    		pw.println("</script>");
		}

	}

}
