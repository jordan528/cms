package controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/addcontact")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)

public class Addcontact extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/*
	 * private static java.sql.Timestamp getCurrentTimeStamp() {
	 * 
	 * java.util.Date today = new java.util.Date(); return new
	 * java.sql.Timestamp(today.getTime()); }
	 */

	public String getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/YYYY  ");
		String strDate = mdformat.format(calendar.getTime());
		return strDate;

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		Part img1 = request.getPart("img1");
		Part img2 = request.getPart("img2");
		Part img3 = request.getPart("img3");
		Part img4 = request.getPart("img4");
		
		String group = request.getParameter("group");
		String catogary = request.getParameter("catogary");
		String refnotes = request.getParameter("refnotes");
		String title = request.getParameter("title");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String nname = request.getParameter("nname");
		String mobile = request.getParameter("mobile");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");

		// personal data
		String pmobile = request.getParameter("pmobile");
		String ptelephone = request.getParameter("ptelephone");
		String pemail = request.getParameter("pemail");
		String pim = request.getParameter("pim");
		String social1 = request.getParameter("socialmedia1");
		String social2 = request.getParameter("socialmedia2");
		String addsline1 = request.getParameter("addsline1");
		String addsline2 = request.getParameter("addsline2");
		String pcity = request.getParameter("pcity");
		String postalcode = request.getParameter("postalcode");
		String pcountry = request.getParameter("pcountry");

		// company data
		String company = request.getParameter("company");
		String desgnation = request.getParameter("desgnation");
		String cmobile = request.getParameter("cmobile");
		String ctelephone = request.getParameter("ctelephone");
		String cemail = request.getParameter("cemail");
		String cim = request.getParameter("cim");
		String csm1 = request.getParameter("csocialmedia1");
		String csm2 = request.getParameter("csocialmedia2");
		String caddsline1 = request.getParameter("caddsline1");
		String caddsline2 = request.getParameter("caddsline2");
		String ccity = request.getParameter("ccity");
		String zipcode = request.getParameter("zipcode");
		String ccountry = request.getParameter("ccountry");
	//	String groupid=request.getParameter("groupid");

		InputStream is = img1.getInputStream();
		InputStream is1 = img2.getInputStream();
		InputStream is2 = img3.getInputStream();
		InputStream is3 = img4.getInputStream();
		

	//	System.out.println(img1 + ":-:" + img1.getSize() + ":--:" + is+groupid);
		Connection con = null;
		PreparedStatement ps = null;
		response.setContentType("text/html");
		PrintWriter pw= response.getWriter();
		try {
			con = controller.dbcon.getConnection();

			ps = con.prepareStatement(
					"INSERT INTO `contacts`(`id`, `img1`, `img2`, `img3`, `img4`, `groups`, `catogary`, `refnotes`, `title`, `fname`, `lname`, `nname`, `mobile`, `telephone`, `email`, `pmobile`, `ptelephone`, `pemail`, `pim`, `socialmedia1`, `socialmedia2`, `addsline1`, `addsline2`, `pcity`, `potalcode`, `pcountry`, `company`, `designation`, `cmobile`, `ctelephone`, `cemail`, `cim`, `csocialmedia1`, `csocialmedia2`, `caddsline1`, `caddsline2`, `ccity`, `zipcode`, `ccountry`,TimeStamp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setInt(1, 0);
			/*
			 * ps.setBinaryStream(2, (InputStream) img1,(int) img1.getSize());
			 * ps.setBinaryStream(3, (InputStream) img2,(int) img2.getSize());
			 * ps.setBinaryStream(4, (InputStream) img3,(int) img3.getSize());
			 * ps.setBinaryStream(5, (InputStream) img4,(int) img4.getSize());
			 */
			
			ps.setBlob(2, is);
			ps.setBlob(3, is1);
			ps.setBlob(4, is2);
			ps.setBlob(5, is3);
			ps.setString(6, group);
			ps.setString(7, catogary);
			ps.setString(8, refnotes);
			ps.setString(9, title);
			ps.setString(10, fname);
			ps.setString(11, lname);
			ps.setString(12, nname);
			ps.setString(13, mobile);
			ps.setString(14, telephone);
			ps.setString(15, email);
			ps.setString(16, pmobile);
			ps.setString(17, ptelephone);
			ps.setString(18, pemail);
			ps.setString(19, pim);
			ps.setString(20, social1);
			ps.setString(21, social2);
			ps.setString(22, addsline1);
			ps.setString(23, addsline2);
			ps.setString(24, pcity);
			ps.setString(25, postalcode);
			ps.setString(26, pcountry);
			ps.setString(27, company);
			ps.setString(28, desgnation);
			ps.setString(29, cmobile);
			ps.setString(30, ctelephone);
			ps.setString(31, cemail);
			ps.setString(32, cim);
			ps.setString(33, csm1);
			ps.setString(34, csm2);
			ps.setString(35, caddsline1);
			ps.setString(36, caddsline2);
			ps.setString(37, ccity);
			ps.setString(38, zipcode);
			ps.setString(39, ccountry);
			ps.setString(40, getCurrentDate());

			int rs = ps.executeUpdate();
	//		System.out.println(rs);
	//		System.out.println(getCurrentDate());

			if (rs > 0) {
				 request.getRequestDispatcher("landingpage.jsp").include(request,response);
				 pw.println("<script type=\"text/javascript\" src=\"assets/jquery-3.1.1.min.js\"></script>");
				 pw.println("<script type=\"text/javascript\" src=\"assets/jquery-confirm.js\"></script>");
				 pw.println("<link rel=\"stylesheet\" href=\"assets/jquery-confirm.css\">");
				 pw.println("<script type=\"text/javascript\">");
				 pw.println("$.alert({title:'Success!',content: 'Successfully Added', type: 'green',buttons: { OK: function(){ location='landingpage.jsp';}} });");
				 pw.println("</script>");
				 
			}

			else {
				 request.getRequestDispatcher("addcontact.jsp").include(request,response);
				
				 pw.println("<script type=\"text/javascript\" src=\"assets/jquery-3.1.1.min.js\"></script>");
				 pw.println("<script type=\"text/javascript\" src=\"assets/jquery-confirm.js\"></script>");
				 pw.println("<link rel=\"stylesheet\" href=\"assets/jquery-confirm.css\">");
				 pw.println("<script type=\"text/javascript\">");
				 pw.println("$.alert({title:'Error!',content: 'Failed.,Try again', type: 'red',buttons: { OK: function(){ location='addcontact.jsp';}} });");
				 pw.println("</script>");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
