package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateGrp
 */
@WebServlet("/updategrp")
public class UpdateGrp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public UpdateGrp() {
        super();
        // TODO Auto-generated constructor stub
    }

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			response.setContentType("text/html");

			String oldgrpname = request.getParameter("oldgrpname");
			String newgrpname = request.getParameter("newgrpname");
			Connection con = null;
			PreparedStatement ps = null;

				try {
					con = controller.dbcon.getConnection();
						ps = con.prepareStatement("UPDATE `groups` SET `grpname`=? WHERE `grpname`=?");
						ps.setString(1, newgrpname);
						ps.setString(2, oldgrpname);
						int rs = ps.executeUpdate();
						System.out.println(rs);
						if (rs > 0) {
							
							ps=con.prepareStatement("UPDATE contacts SET groups=? where groups=?");
							ps.setString(1, newgrpname);
							ps.setString(2, oldgrpname);
							ps.executeUpdate();
						//	response.sendRedirect("addcontact.jsp");
							response.getWriter().write("Group Name Updated Successfully");
						}else{
						//	response.sendRedirect("addcontact.jsp");
							response.getWriter().write("Failed..Try again..");
						}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
	
	}

}
