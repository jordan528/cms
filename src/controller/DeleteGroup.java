package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
@WebServlet("/deleteGroup")
public class DeleteGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//System.out.println("called");
		Connection con = null;
		PreparedStatement ps = 	null;
//		int grpid= Integer.parseInt(request.getParameter("id"));
		String grpname= request.getParameter("grpname");
//		System.out.println(grpid+":::"+grpname);
		try {
			con=dbcon.getConnection();

			ps = con.prepareStatement("select * from contacts where groups=?");
			ps.setString(1, grpname);
			
			ResultSet rs = ps.executeQuery();
			 if(rs.next()) {
				 response.getWriter().write("exist");
			 }
			 else{
				 PreparedStatement pst= con.prepareStatement("delete from groups where grpname=?");
				 pst.setString(1, grpname);
				// pst.setInt(2, grpid);
				 
				 int r= pst.executeUpdate();
				
				 if(r>0){
					 response.getWriter().write("yes");
				 }
				 else{
					 response.getWriter().write("no");
				 }
			 }

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 

	}

}
