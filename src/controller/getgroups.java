package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import pojo.groupbean;

/**
 * Servlet implementation class getgroups
 */
@WebServlet("/getgroups")
public class getgroups extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//System.out.println("called");
		Connection con = null;
		PreparedStatement ps = 	null;
		ArrayList<groupbean> al = new ArrayList<groupbean>();
		try {
			con=dbcon.getConnection();

			ps = con.prepareStatement("select * from groups");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				groupbean bean = new groupbean();
				bean.setGrpname(rs.getString("grpname"));

				al.add(bean);
			//	request.setAttribute("groups", al);

			}

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson g= new Gson();
		JsonElement je=g.toJsonTree(al,new TypeToken<List<groupbean>>() {}.getType());
	//	String json= g.toJson(al);
	//	String s= "[".concat(json).concat("]");
		JsonArray json=je.getAsJsonArray();
		response.setContentType("application/json");
	//	System.out.println(json);
		response.getWriter().print(json);
		

	}

}
