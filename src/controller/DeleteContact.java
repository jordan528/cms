package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
@WebServlet("/deleteContact")
public class DeleteContact extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Connection con = null;
		PreparedStatement ps = 	null;
		int id= Integer.parseInt(request.getParameter("id"));

		try {
			con=dbcon.getConnection();

			
			 ps= con.prepareStatement("delete from contacts where id=?");
			 ps.setInt(1, id);
			 
			 int r= ps.executeUpdate();
			
			 if(r>0){
				 response.getWriter().write("Contact Deleted Successfully");
			 }
			 else{
				 response.getWriter().write("Failed., Try again..");
			 }
			 

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		 

	}

}
