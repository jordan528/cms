package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCategory
 */
@WebServlet("/AddCategory")
public class AddCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddCategory() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		String catname = request.getParameter("catname");

		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = controller.dbcon.getConnection();

			ps = con.prepareStatement("INSERT INTO `category`(`sno`, `categoryname`) VALUES (?,?)");
			ps.setInt(1, 0);
			ps.setString(2, catname);

			int rs = ps.executeUpdate();
			System.out.println(rs);
			if (rs > 0) {
				// request.getRequestDispatcher("index.jsp").forward(request,
				// response);
			//	response.sendRedirect("addcontact.jsp");
				response.getWriter().write("Category Added Successfully");
			}else{
				//	response.sendRedirect("addcontact.jsp");
				response.getWriter().write("Failed..Try again..");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
