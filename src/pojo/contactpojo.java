package pojo;

import java.sql.Blob;

public class contactpojo {

	public int sno;
	public Blob img1;
	public Blob img2;
	public Blob img3;
	public Blob img4;
	String grpname,catname;
	
	public String getGrpname() {
		return grpname;
	}

	public void setGrpname(String grpname) {
		this.grpname = grpname;
	}

	public String getCatname() {
		return catname;
	}

	public void setCatname(String catname) {
		this.catname = catname;
	}

	public String name;
	public String title;
	public String firstname;
	public String lastname;
	public String nickname;
	public String contmobile;
	public String telephone;
	public String contemail;
	public String refnotes;

	public String getRefnotes() {
		return refnotes;
	}

	public void setRefnotes(String refnotes) {
		this.refnotes = refnotes;
	}

	public String prsnlmoble;
	public String prsnltelephone;
	public String prsnlemail;
	public String prsnlim;
	

	public String socialmedia1;
	public String socialmedia2;
	public String addsline1;
	public String addsline2;
	public String city;
	public String postalcode;
	public String country;
	public String cmpnymobile;
	public String cmpnytelephone;
	public String cmpnyemail;
	public String cmpnyim;
	public String cmpnysocialmedia1;
	public String cmpnysocialmedia2;
	public String cmpnyaddsline1;
	public String cmpnyaddsline2;
	public String cmpnycity;
	public String cmpnyzipcode;
	public String cmpnycountry;

	public String company;
	public String designation;
	public String mobile;
	public String email;

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public Blob getImg1() {
		return img1;
	}

	public void setImg1(Blob img1) {
		this.img1 = img1;
	}

	public Blob getImg2() {
		return img2;
	}

	public void setImg2(Blob img2) {
		this.img2 = img2;
	}

	public Blob getImg3() {
		return img3;
	}

	public void setImg3(Blob img3) {
		this.img3 = img3;
	}

	public Blob getImg4() {
		return img4;
	}

	public void setImg4(Blob img4) {
		this.img4 = img4;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getContmobile() {
		return contmobile;
	}

	public void setContmobile(String contmobile) {
		this.contmobile = contmobile;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getContemail() {
		return contemail;
	}

	public void setContemail(String contemail) {
		this.contemail = contemail;
	}

	public String getPrsnlmoble() {
		return prsnlmoble;
	}

	public void setPrsnlmoble(String prsnlmoble) {
		this.prsnlmoble = prsnlmoble;
	}

	public String getPrsnltelephone() {
		return prsnltelephone;
	}

	public void setPrsnltelephone(String prsnltelephone) {
		this.prsnltelephone = prsnltelephone;
	}

	public String getPrsnlemail() {
		return prsnlemail;
	}

	public void setPrsnlemail(String prsnlemail) {
		this.prsnlemail = prsnlemail;
	}

	public String getSocialmedia1() {
		return socialmedia1;
	}

	public void setSocialmedia1(String socialmedia1) {
		this.socialmedia1 = socialmedia1;
	}

	public String getSocialmedia2() {
		return socialmedia2;
	}

	public void setSocialmedia2(String socialmedia2) {
		this.socialmedia2 = socialmedia2;
	}

	public String getAddsline1() {
		return addsline1;
	}

	public void setAddsline1(String addsline1) {
		this.addsline1 = addsline1;
	}

	public String getAddsline2() {
		return addsline2;
	}

	public void setAddsline2(String addsline2) {
		this.addsline2 = addsline2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCmpnymobile() {
		return cmpnymobile;
	}

	public void setCmpnymobile(String cmpnymobile) {
		this.cmpnymobile = cmpnymobile;
	}

	public String getCmpnytelephone() {
		return cmpnytelephone;
	}

	public void setCmpnytelephone(String cmpnytelephone) {
		this.cmpnytelephone = cmpnytelephone;
	}

	public String getCmpnyemail() {
		return cmpnyemail;
	}

	public void setCmpnyemail(String cmpnyemail) {
		this.cmpnyemail = cmpnyemail;
	}

	public String getCmpnyim() {
		return cmpnyim;
	}

	public void setCmpnyim(String cmpnyim) {
		this.cmpnyim = cmpnyim;
	}

	public String getCmpnysocialmedia1() {
		return cmpnysocialmedia1;
	}

	public void setCmpnysocialmedia1(String cmpnysocialmedia1) {
		this.cmpnysocialmedia1 = cmpnysocialmedia1;
	}

	public String getCmpnysocialmedia2() {
		return cmpnysocialmedia2;
	}

	public void setCmpnysocialmedia2(String cmpnysocialmedia2) {
		this.cmpnysocialmedia2 = cmpnysocialmedia2;
	}

	public String getCmpnyaddsline1() {
		return cmpnyaddsline1;
	}

	public void setCmpnyaddsline1(String cmpnyaddsline1) {
		this.cmpnyaddsline1 = cmpnyaddsline1;
	}

	public String getCmpnyaddsline2() {
		return cmpnyaddsline2;
	}

	public void setCmpnyaddsline2(String cmpnyaddsline2) {
		this.cmpnyaddsline2 = cmpnyaddsline2;
	}

	public String getCmpnycity() {
		return cmpnycity;
	}

	public void setCmpnycity(String cmpnycity) {
		this.cmpnycity = cmpnycity;
	}

	public String getCmpnyzipcode() {
		return cmpnyzipcode;
	}

	public void setCmpnyzipcode(String cmpnyzipcode) {
		this.cmpnyzipcode = cmpnyzipcode;
	}

	public String getCmpnycountry() {
		return cmpnycountry;
	}

	public void setCmpnycountry(String cmpnycountry) {
		this.cmpnycountry = cmpnycountry;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getPrsnlim() {
		return prsnlim;
	}

	public void setPrsnlim(String prsnlim) {
		this.prsnlim = prsnlim;
	}
	
	

}
